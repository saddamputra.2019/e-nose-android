import 'package:shared_preferences/shared_preferences.dart';

class Prefs {
  static const _baseUrlKey = 'baseUrl';
  static late SharedPreferences prefs;
  static Future init() async {
    prefs = await SharedPreferences.getInstance();
  }

  static Future setPrefsBaseUrl(String baseUrl) async {
    await prefs.setString(_baseUrlKey, baseUrl);
  }

  static String? getPrefsBaseUrl() {
    return prefs.getString(_baseUrlKey);
  }
}
