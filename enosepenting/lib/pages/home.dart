import 'dart:async';

import 'package:enosepenting/api.dart';
import 'package:enosepenting/helpers/charts_helper.dart';
import 'package:enosepenting/model/arima.dart';
import 'package:enosepenting/model/files.dart';
import 'package:enosepenting/model/node.dart';
import 'package:enosepenting/prefs.dart';
// import 'package:enosepenting/pages/node.dart';
import 'package:enosepenting/widget/node_button.dart';
import 'package:enosepenting/widget/settings.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'dart:math';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Node> listNode = [];
  List<Arima> listArima = [];
  String? klasifikasiNode1;
  List<Files> node1 = [];
  bool autoRefresh = false;

  Timer? ticker;

  /// -- untuk menjalankan fitur autoRefresh tiap 1 detik
  startAutoRefresh() {
    ticker = Timer.periodic(const Duration(seconds: 1), (timer) {
      getData();
    });
  }

  /// -- untuk menghentikan fitur autoRefresh
  stopAutoRefresh() {
    if (ticker?.isActive ?? false) {
      ticker?.cancel();
    }
  }


  /// -- untuk mengambil data arima terbaru berdasarkan node_id dari listArima yang terisi oleh API arima di baris :61
  Arima? latestArimaByNodeId(int nodeId) {
    final dataArimaPerNode = listArima.where((e) => e.nodeId == nodeId);
    if (dataArimaPerNode.isEmpty) return null;
    // -- ini mengambil data terakhir (last) / terbaru
    return dataArimaPerNode.last;
  }

  Future getData() async {
    listNode = await Api.getNodeData();

    /// diurutkan berdasarkan waktu dan tanggal dari terkecil
    listNode.sort((a, b) => a.timestamp!.compareTo(b.timestamp!));

    listArima = await Api.getArima();

    /// -- ini ngambil klasifikasi untuk node 1 saja
    klasifikasiNode1 = await Api.getKlasifikasi();

    /// -- ini ngambil data arima dari node 1 dengan fungsi [latestArimaByNodeId(int nodeId)] di baris :56
    final arimaNode1 = latestArimaByNodeId(1);

    /// -- lalu dibuat ke grafik bar
    node1 = [
      Files(
        'arima1',
        arimaNode1!.arima1!, // ini data arima 1
        Color.fromARGB(255, 6, 102, 228),
      ),
      Files(
        'arima2',
        arimaNode1.arima2!, // ini data arima 2
        Color.fromARGB(255, 6, 102, 228),
      ),
      Files(
        'arima3',
        arimaNode1.arima3!, // ini data arima 3
        Color.fromARGB(255, 6, 102, 228),
      ),
      Files(
        'arima4',
        arimaNode1.arima4!, //ini data arima 4
        Color.fromARGB(255, 6, 102, 228),
      ),
      Files(
        'arima5',
        arimaNode1.arima5!, // ini data arima 5
        Color.fromARGB(255, 6, 102, 228),
      ),
      Files(
        'arima6',
        arimaNode1.arima6!, // ini data arima 6
        Color.fromARGB(255, 6, 102, 228),
      ),
    ];
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    if (Prefs.getPrefsBaseUrl() == null) {
      openSettings();
    } else {
      getData();
    }
  }

  @override
  Widget build(BuildContext context) {
    // var node2 = [
    //   Files("Senin", 150, Color.fromARGB(255, 236, 63, 63)),
    //   Files("Selasa", 350, Color.fromARGB(255, 236, 63, 63)),
    //   Files("Rabu", 150, Color.fromARGB(255, 236, 63, 63)),
    //   Files("Kamis", 250, Color.fromARGB(255, 236, 63, 63)),
    //   Files("Jumat", 300, Color.fromARGB(255, 236, 63, 63)),
    //   Files("Sabtu", 1100, Color.fromARGB(255, 236, 63, 63)),
    //   Files("Minggu", 3100, Color.fromARGB(255, 236, 63, 63)),
    // ];

    // var node3 = [
    //   Files("Senin", 400, Color.fromARGB(255, 248, 128, 36)),
    //   Files("Selasa", 300, Color.fromARGB(255, 248, 128, 36)),
    //   Files("Rabu", 200, Color.fromARGB(255, 248, 128, 36)),
    //   Files("Kamis", 250, Color.fromARGB(255, 248, 128, 36)),
    //   Files("Jumat", 100, Color.fromARGB(255, 248, 128, 36)),
    //   Files("Sabtu", 1400, Color.fromARGB(255, 248, 128, 36)),
    //   Files("Minggu", 3500, Color.fromARGB(255, 248, 128, 36)),
    // ];

    var series = [
      charts.Series(
        domainFn: (Files files, _) => files.file,
        measureFn: (Files files, _) => files.nilai,
        colorFn: (Files files, _) => files.color,
        id: 'Node 1',
        seriesColor: materialColorToChartColor(
          const Color.fromARGB(255, 6, 102, 228),
        ),
        data: node1,
      ),
      // charts.Series(
      //   domainFn: (Files files, _) => files.file,
      //   measureFn: (Files files, _) => files.nilai,
      //   colorFn: (Files files, _) => files.color,
      //   id: 'Files',
      //   // data: node2,
      // ),
      charts.Series(
        domainFn: (files, _) => '',
        measureFn: (files, _) => 0,
        id: 'Node 2',
        seriesColor: materialColorToChartColor(
          const Color.fromARGB(255, 236, 63, 63),
        ),
        data: [],
        // data: node3,
      ),
      charts.Series(
        domainFn: (files, _) => '',
        measureFn: (files, _) => 0,
        id: 'Node 3',
        seriesColor: materialColorToChartColor(
          const Color.fromARGB(255, 248, 128, 36),
        ),
        data: [],
        // data: node3,
      ),
    ];

    var chart = charts.BarChart(
      series,
      barGroupingType: charts.BarGroupingType.grouped,
      behaviors: [charts.SeriesLegend()],
    );

    return Scaffold(
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 10, 210, 127),
          title: const Text(
            "E-Nose",
            style: TextStyle(color: Colors.black),
          ),
          actions: [
            Switch(
                value: autoRefresh,
                onChanged: (Prefs.getPrefsBaseUrl() ?? '').isEmpty
                    ? null
                    : (val) {
                        if (val) {
                          startAutoRefresh();
                        } else {
                          stopAutoRefresh();
                        }
                        setState(() {
                          autoRefresh = val;
                        });
                      }),
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () async {
                await openSettings();
              },
            ),
          ],
        ),
        body: RefreshIndicator(
          onRefresh: () {
            return getData();
          },
          child: ListView(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  color: const Color.fromARGB(255, 10, 210, 127),
                ),
                margin: const EdgeInsets.fromLTRB(3.0, 8.0, 3.0, 8.0),
                height: 70,
                child: const Center(
                  child: Text(
                    "Data Sensor E-Nose",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 23,
                        fontWeight: FontWeight.w800),
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7),
                    color: const Color.fromARGB(255, 233, 90, 90)),
                margin: const EdgeInsets.fromLTRB(3.0, 0, 3.0, 8.0),
                height: 70,
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: const Center(
                  child: Text(
                    "Menjuh Jika Polusi Udara Melebihi 40%",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w800),
                  ),
                ),
              ),
              GridView.count(
                physics: const NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.symmetric(horizontal: 5),
                shrinkWrap: true,
                childAspectRatio: 4 / 4,
                crossAxisCount: 3,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                children: [
                  /// -- ini tombol node 1
                  NodeButton(
                    nodeId: 1,
                    nodes: listNode,

                    /// ini data dari api /arima Ref {./lib/api.dart:71}
                    arima: latestArimaByNodeId(1),

                    /// data dari klasifikasi
                    klasifikasi: klasifikasiNode1,
                  ),

                  /// -- ini tombol node 2
                  NodeButton(
                    nodeId: 2,
                    nodes: listNode,

                    /// hardcode klasifikasi
                    klasifikasi: 'Udara Bersih',
                  ),

                  /// -- ini tombol node 3
                  NodeButton(
                    nodeId: 3,
                    nodes: listNode,

                    /// ini data hardcode
                    klasifikasi: 'Udara Kotor',
                  ),
                ],
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color.fromARGB(255, 216, 220, 218),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 1,
                        offset: Offset(0, 5),
                      )
                    ]),
                margin: EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color.fromARGB(255, 10, 210, 127),
                      ),
                      height: 50,
                      child: Center(
                          child: Text("Table Array Sensor E-Nose",
                              style: TextStyle(
                                  color: Colors.white, fontSize: 20))),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(5.0, 1.5, 5.0, 1.5),
                            height: 100,
                            color: Color.fromARGB(255, 249, 249, 249),
                            child: Center(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Tingkat Keamanan",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w800),
                                    )
                                  ]),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(5.0, 1.5, 0, 1.5),
                            height: 100,
                            color: Color.fromARGB(255, 249, 249, 249),
                            child: Center(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("Kotor",
                                        style: TextStyle(
                                            color: Color.fromARGB(
                                                255, 255, 50, 36),
                                            fontSize: 18,
                                            fontWeight: FontWeight.w800))
                                  ]),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(0, 1.5, 5.0, 1.5),
                            height: 100,
                            color: Color.fromARGB(255, 249, 249, 249),
                            child: Center(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("Bersih",
                                        style: TextStyle(
                                            color: Color.fromARGB(
                                                255, 0, 255, 178),
                                            fontSize: 18,
                                            fontWeight: FontWeight.w800))
                                  ]),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color.fromARGB(255, 216, 220, 218),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 1,
                        offset: Offset(0, 5),
                      )
                    ]),
                margin: EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color.fromARGB(255, 10, 210, 127),
                      ),
                      height: 50,
                      child: Center(
                          child: Text("Grafik Array Sensor",
                              style: TextStyle(
                                  color: Colors.white, fontSize: 20))),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(5.0, 1.5, 0, 1.5),
                            height: 50,
                            color: Color.fromARGB(255, 6, 102, 228),
                            //child
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(1.5, 1.5, 0, 1.5),
                            height: 50,
                            color: Color.fromARGB(255, 236, 63, 63),
                            //child
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(1.5, 1.5, 5.0, 1.5),
                            height: 50,
                            color: Color.fromARGB(255, 248, 128, 36),
                            //child
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(5.0, 1.5, 0, 0),
                            height: 50,
                            color: Color.fromARGB(255, 249, 249, 249),
                            child: Center(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("NODE 1",
                                        style: TextStyle(
                                            color: Color.fromARGB(
                                                255, 6, 102, 228),
                                            fontSize: 15,
                                            fontWeight: FontWeight.w800))
                                  ]),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(0, 1.5, 0, 0),
                            height: 50,
                            color: Color.fromARGB(255, 249, 249, 249),
                            child: Center(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("NODE 2",
                                        style: TextStyle(
                                            color: Color.fromARGB(
                                                255, 236, 63, 63),
                                            fontSize: 15,
                                            fontWeight: FontWeight.w800))
                                  ]),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(0, 1.5, 5.0, 0),
                            height: 50,
                            color: Color.fromARGB(255, 249, 249, 249),
                            child: Center(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("NODE 3",
                                        style: TextStyle(
                                            color: Color.fromARGB(
                                                255, 248, 128, 36),
                                            fontSize: 15,
                                            fontWeight: FontWeight.w800))
                                  ]),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Color.fromARGB(255, 249, 249, 249),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 1,
                              blurRadius: 1,
                              offset: Offset(0, 5),
                            )
                          ]),
                      margin: EdgeInsets.fromLTRB(5.0, 0, 5.0, 1.5),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 275,
                              child: chart,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  openSettings() async {
    await showDialog(
      context: context,
      builder: (context) => Dialog(
        child: Settings(),
      ),
    );
    print('baseUrl ${Prefs.getPrefsBaseUrl()}');
    if ((Prefs.getPrefsBaseUrl() ?? '').isEmpty) {
      //
      print('kesini');
    } else {
      getData();
    }
    setState(() {});
  }
}
