import 'package:enosepenting/model/node.dart';
import 'package:enosepenting/widget/charts.dart';
import 'package:flutter/material.dart';

class NodePages extends StatefulWidget {
  const NodePages({
    Key? key,
    required this.nodeId,
    required this.perNode,
  }) : super(key: key);

  final int nodeId;
  final List<Node> perNode;

  @override
  State<NodePages> createState() => _NodePagesState();
}

class _NodePagesState extends State<NodePages> {
  String get nodeId => widget.nodeId.toString();

  List<Node> get nodes => widget.perNode.take(6).toList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 10, 210, 127),
        title: Text(
          "Node $nodeId",
          style: const TextStyle(color: Colors.black),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.all(10),
        children: [
          Charts(
            title: 'Sensor 1',
            nodes: nodes,
            valueFn: (n) => n.sensor1,
          ),
          Charts(
            title: 'Sensor 2',
            nodes: nodes,
            valueFn: (n) => n.sensor2,
          ),
          Charts(
            title: 'Sensor 3',
            nodes: nodes,
            valueFn: (n) => n.sensor3,
          ),
          Charts(
            title: 'Sensor 4',
            nodes: nodes,
            valueFn: (n) => n.sensor4,
          ),
          Charts(
            title: 'Sensor 5',
            nodes: nodes,
            valueFn: (n) => n.sensor5,
          ),
          Charts(
            title: 'Sensor 6',
            nodes: nodes,
            valueFn: (n) => n.sensor6,
          ),
        ],
      ),
    );
  }
}
