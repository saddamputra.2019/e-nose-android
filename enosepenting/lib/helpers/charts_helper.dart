import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

charts.Color materialColorToChartColor(Color color) {
  return charts.Color(
    r: color.red,
    g: color.green,
    b: color.blue,
  );
}
