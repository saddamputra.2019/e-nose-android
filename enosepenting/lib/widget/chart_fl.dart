/// Example of a time series chart with an end points domain axis.
///
/// An end points axis generates two ticks, one at each end of the axis range.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class EndPointsAxisTimeSeriesChart extends StatelessWidget {
  final List<charts.Series<dynamic, DateTime>> seriesList;
  final bool animate;
  final List<charts.ChartBehavior<DateTime>>? behaviors;

  const EndPointsAxisTimeSeriesChart(
    this.seriesList, {
    Key? key,
    this.animate = false,
    this.behaviors,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return charts.TimeSeriesChart(
      seriesList,
      animate: animate,
      domainAxis: const charts.EndPointsTimeAxisSpec(),
      defaultRenderer: charts.LineRendererConfig(),
      customSeriesRenderers: [
        charts.PointRendererConfig(
            // ID used to link series to this renderer.
            customRendererId: 'customPoint')
      ],
      behaviors: behaviors,
    );
  }
}
