import 'package:enosepenting/helpers/charts_helper.dart';
import 'package:enosepenting/model/node.dart';
import 'package:enosepenting/widget/chart_fl.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class Charts extends StatelessWidget {
  const Charts({
    Key? key,
    this.title = '',
    required this.nodes,
    required this.valueFn,
  }) : super(key: key);

  final String title;
  final List<Node> nodes;
  final dynamic Function(Node node) valueFn;

  @override
  Widget build(BuildContext context) {
    final data = nodes
        .map((e) =>
            {'value': valueFn(e), 'timestamp': DateTime.parse(e.timestamp!)})
        .toList();

    return Card(
      child: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            Text(
              title,
              style: const TextStyle(
                fontSize: 20,
              ),
            ),
            const Divider(
              thickness: 2,
              height: 20,
              color: Color.fromARGB(255, 10, 210, 127),
            ),
            AspectRatio(
              aspectRatio: 4 / 3,
              child: EndPointsAxisTimeSeriesChart(
                [
                  charts.Series<Map<String, dynamic>, DateTime>(
                    domainFn: (data, index) => data['timestamp'],
                    measureFn: (data, index) => data['value'],
                    colorFn: (data, index) => materialColorToChartColor(
                        const Color.fromARGB(255, 10, 210, 127)),
                    id: UniqueKey().toString(),
                    data: data,
                  ),
                  charts.Series<Map<String, dynamic>, DateTime>(
                    domainFn: (data, index) => data['timestamp'],
                    measureFn: (data, index) => data['value'],
                    colorFn: (data, index) =>
                        materialColorToChartColor(Colors.deepOrange),
                    id: UniqueKey().toString(),
                    data: data,
                  )..setAttribute(charts.rendererIdKey, 'customPoint'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
