import 'package:enosepenting/prefs.dart';
import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  Settings({Key? key}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  TextEditingController controllerBaseUrl = TextEditingController(
    text: Prefs.getPrefsBaseUrl(),
  );

  @override
  void dispose() {
    controllerBaseUrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'PENGATURAN',
            style: Theme.of(context).textTheme.headline5,
          ),
          const SizedBox(height: 10),
          TextField(
            controller: controllerBaseUrl,
            decoration: InputDecoration(
              label: Text('Base URL'),
              hintText: 'Base URL (http://localhost:10000/api/node)',
            ),
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TextButton(
                onPressed: () async {
                  Navigator.pop(context);
                },
                child: Text('BATAL'),
              ),
              const SizedBox(width: 10),
              ElevatedButton(
                onPressed: () async {
                  await Prefs.setPrefsBaseUrl(controllerBaseUrl.text);
                  Navigator.pop(context);
                },
                child: Text('SIMPAN'),
              ),
            ],
          )
        ],
      ),
    );
  }
}
