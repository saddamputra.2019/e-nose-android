import 'package:enosepenting/model/arima.dart';
import 'package:enosepenting/model/node.dart';
import 'package:enosepenting/pages/node.dart';
import 'package:flutter/material.dart';

class NodeButton extends StatelessWidget {
  const NodeButton({
    Key? key,
    required this.nodeId,
    required this.nodes,
    this.arima,
    this.klasifikasi,
  }) : super(key: key);

  final int nodeId;
  final Arima? arima;
  final List<Node> nodes;
  final String? klasifikasi;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.zero,
        visualDensity: VisualDensity.compact,
        primary: Colors.cyan,
      ),
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => NodePages(
              perNode: nodes.where((f) => f.node_id == nodeId).toList(),
              nodeId: nodeId,
            ),
          ),
        );
      },
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 20),
        alignment: Alignment.center,
        child: Column(
          children: [
            Text(
              'Node $nodeId',
              style: const TextStyle(
                fontSize: 20,
              ),
            ),
            const SizedBox(height: 5),

            /// ini data klasifikasi
            if (klasifikasi != null)
              Text(
                klasifikasi ?? '',
                style: const TextStyle(
                  fontSize: 16,
                  // color: Color.fromARGB(255, 10, 210, 127),
                ),
              ),
            const SizedBox(height: 10),

            /// ini data arima
            if (arima != null)
              Text(
                arima!.klasifikasi ?? '',
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.red,
                ),
              ),
            // Text(
            //   '${arima?.id ?? '-'}',
            //   style: const TextStyle(
            //     fontSize: 16,
            //     // color: Color.fromARGB(255, 10, 210, 127),
            //   ),
            // ),
            // Text(
            //   '${calculatePercentage(e.id!).toStringAsFixed(2)}%',
            //   style: TextStyle(
            //     fontSize: 20,
            //     fontWeight: FontWeight.bold,
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
