import 'package:enosepenting/pages/home.dart';
import 'package:enosepenting/prefs.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:fluttertoast/fluttertoast.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Prefs.init(); /// -- inisialisasi Shared Preference untuk menyimpan data di perangkat mobile

  /// -- menjalankan aplikasi flutter MaterialApp()
  runApp(
    MaterialApp(
      title: "E-Nose Application",
      /// -- tampilan awal aplikasi adalah HomePage() di file pages/home.dart
      home: HomePage(),
    ),
  );
}
