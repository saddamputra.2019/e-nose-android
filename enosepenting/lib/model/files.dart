import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class Files {
  final String file;
  final double nilai;
  final charts.Color color;

  Files(this.file, this.nilai, Color color)
      : color = charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}
