// ignore_for_file: non_constant_identifier_names

class Node {
  int? id;
  double? sensor1;
  double? sensor2;
  double? sensor3;
  double? sensor4;
  double? sensor5;
  double? sensor6;
  String? timestamp;
  int? node_id;

  Node({
    this.id,
    this.sensor1,
    this.sensor2,
    this.sensor3,
    this.sensor4,
    this.sensor5,
    this.sensor6,
    this.timestamp,
    this.node_id,
  });

  Node.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sensor1 = double.parse('${json['sensor1']}');
    sensor2 = double.parse('${json['sensor2']}');
    sensor3 = double.parse('${json['sensor3']}');
    sensor4 = double.parse('${json['sensor4']}');
    sensor5 = double.parse('${json['sensor5']}');
    sensor6 = double.parse('${json['sensor6']}');
    timestamp = DateTime.parse(json['timestamp']).toIso8601String();
    node_id = json['nodeId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['sensor1'] = this.sensor1;
    data['sensor2'] = this.sensor2;
    data['sensor3'] = this.sensor3;
    data['sensor4'] = this.sensor4;
    data['sensor5'] = this.sensor5;
    data['sensor6'] = this.sensor6;
    data['timestamp'] = this.timestamp;
    data['node_id'] = this.node_id;
    return data;
  }

  Node copyWith({
    int? id,
    double? sensor1,
    double? sensor2,
    double? sensor3,
    double? sensor4,
    double? sensor5,
    double? sensor6,
    String? timestamp,
    int? node_id,
  }) {
    return Node(
      id: id ?? this.id,
      sensor1: sensor1 ?? this.sensor1,
      sensor2: sensor2 ?? this.sensor2,
      sensor3: sensor3 ?? this.sensor3,
      sensor4: sensor4 ?? this.sensor4,
      sensor5: sensor5 ?? this.sensor5,
      sensor6: sensor6 ?? this.sensor6,
      timestamp: timestamp ?? this.timestamp,
      node_id: node_id ?? this.node_id,
    );
  }
}
