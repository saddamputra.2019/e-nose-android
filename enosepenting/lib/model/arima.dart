import 'dart:convert';

class Arima {
  final int? id;
  final int? nodeId;
  final double? arima1;
  final double? arima2;
  final double? arima3;
  final double? arima4;
  final double? arima5;
  final double? arima6;
  final String? klasifikasi;
  Arima({
    this.id,
    this.nodeId,
    this.arima1,
    this.arima2,
    this.arima3,
    this.arima4,
    this.arima5,
    this.arima6,
    this.klasifikasi,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'node_id': nodeId,
      'arima1': arima1,
      'arima2': arima2,
      'arima3': arima3,
      'arima4': arima4,
      'arima5': arima5,
      'arima6': arima6,
      'klasifikasi': klasifikasi,
    };
  }

  factory Arima.fromJson(Map<String, dynamic> map) {
    return Arima(
      id: map['id']?.toInt() ?? 0,
      nodeId: map['node_id']?.toInt() ?? 0,
      arima1: map['arima1']?.toDouble() ?? 0.0,
      arima2: map['arima2']?.toDouble() ?? 0.0,
      arima3: map['arima3']?.toDouble() ?? 0.0,
      arima4: map['arima4']?.toDouble() ?? 0.0,
      arima5: map['arima5']?.toDouble() ?? 0.0,
      arima6: map['arima6']?.toDouble() ?? 0.0,
      klasifikasi: map['klasifikasi'] ?? '',
    );
  }

  Arima copyWith({
    int? id,
    int? nodeId,
    double? arima1,
    double? arima2,
    double? arima3,
    double? arima4,
    double? arima5,
    double? arima6,
    String? klasifikasi,
  }) {
    return Arima(
      id: id ?? this.id,
      nodeId: nodeId ?? this.nodeId,
      arima1: arima1 ?? this.arima1,
      arima2: arima2 ?? this.arima2,
      arima3: arima3 ?? this.arima3,
      arima4: arima4 ?? this.arima4,
      arima5: arima5 ?? this.arima5,
      arima6: arima6 ?? this.arima6,
      klasifikasi: klasifikasi ?? this.klasifikasi,
    );
  }
}
