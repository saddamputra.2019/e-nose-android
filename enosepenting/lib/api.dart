import 'dart:convert';

import 'package:enosepenting/model/arima.dart';
import 'package:enosepenting/model/node.dart';
import 'package:enosepenting/prefs.dart';
import 'package:http/http.dart' as http;

class Api {
  Api._(); // anonymous class

  /// -- baseUrl untuk mengakses API backend
  static String? get baseUrl => Prefs.getPrefsBaseUrl();

  static getNodeData() async {
    try {
      final response = await http.get(Uri.parse('$baseUrl/node'));

      if (response.statusCode == 200) {
        final data = jsonDecode(response.body)['data'];
        // -- ini data sensors
        Iterable listSensor = data['sensors'];
        final arima =
            (data['arima'] as List).map((e) => double.parse('$e')).toList();

        // -- masukan data dari respon api sensor ke output
        var output = listSensor.map((e) => Node.fromJson(e)).toList();

        // -- ini data prediksi / data ke 5 di masukan ke output dari api
        output.add(
          Node(
            sensor1: arima[0],
            sensor2: arima[1],
            sensor3: arima[2],
            sensor4: arima[3],
            sensor5: arima[4],
            sensor6: arima[5],
            // node id ????

            // -- timestamp dari arima itu mengambil dari data ke-4 sensor ditambah 1 jam
            /// [String] timestamp diubah menjadi [DateTime] lalu ditambahkan dengan .add()
            timestamp: DateTime.parse(output.last.timestamp!)
                .add(const Duration(hours: 1))

                /// setelah ditambahkan di ubah lagi ke [String] dengan format iso8601 atau standarisasi tanggal dan waktu
                .toIso8601String(),
          ),
        );

        // -- semua data yang ada di output
        // -- hardcode output ke node id = 1
        // [DUMMY] node_id
        output = output
            .map((dataSensor) => dataSensor.copyWith(node_id: 1))
            .toList();

        // -- dummy output data sensor node id = 2 duplikat dari data sensor node 1
        // output.addAll(output.map((e) => e.copyWith(node_id: 2)).toList());
        // output.addAll(output.map((e) => e.copyWith(node_id: 3)).toList());
        return output;
      }
    } catch (e, st) {
      print(e);
      print(st);
    }
  }

  static getArima() async {
    try {
      final response = await http.get(Uri.parse('$baseUrl/arima'));
      if (response.statusCode == 200) {
        Iterable list = jsonDecode(response.body)['data'];
        // -- setelah data dari api di masukan ke model arima dengan node id = 1
        // menjadi output
        var output =
            list.map((e) => Arima.fromJson(e).copyWith(nodeId: 1)).toList();

        // -- ini untuk data dummy node 2
        output.add(
          Arima(nodeId: 2, klasifikasi: 'Udara Bersih'),
        );
        output.add(
          Arima(nodeId: 3, klasifikasi: 'Udara Kotor'),
        );
        return output;
      }
    } catch (e) {
      print(e);
    }
  }

  static getKlasifikasi() async {
    try {
      final response = await http.get(Uri.parse('$baseUrl/klasifikasi'));
      if (response.statusCode == 200) {
        Iterable list = jsonDecode(response.body)['data'];
        var output = list.map((e) => e['klasifikasi']).toList().first;
        return output;
      }
    } catch (e) {
      print(e);
    }
  }
}
